package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.CoopcycleApp;
import com.mycompany.myapp.domain.AnUser;
import com.mycompany.myapp.repository.AnUserRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AnUserResource} REST controller.
 */
@SpringBootTest(classes = CoopcycleApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class AnUserResourceIT {

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final String DEFAULT_LOGIN = "AAAAAAAAAA";
    private static final String UPDATED_LOGIN = "BBBBBBBBBB";

    private static final String DEFAULT_PASSWORD = "AAAAAAAAAA";
    private static final String UPDATED_PASSWORD = "BBBBBBBBBB";

    @Autowired
    private AnUserRepository anUserRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAnUserMockMvc;

    private AnUser anUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnUser createEntity(EntityManager em) {
        AnUser anUser = new AnUser()
            .userId(DEFAULT_USER_ID)
            .login(DEFAULT_LOGIN)
            .password(DEFAULT_PASSWORD);
        return anUser;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnUser createUpdatedEntity(EntityManager em) {
        AnUser anUser = new AnUser()
            .userId(UPDATED_USER_ID)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);
        return anUser;
    }

    @BeforeEach
    public void initTest() {
        anUser = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnUser() throws Exception {
        int databaseSizeBeforeCreate = anUserRepository.findAll().size();

        // Create the AnUser
        restAnUserMockMvc.perform(post("/api/an-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anUser)))
            .andExpect(status().isCreated());

        // Validate the AnUser in the database
        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeCreate + 1);
        AnUser testAnUser = anUserList.get(anUserList.size() - 1);
        assertThat(testAnUser.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testAnUser.getLogin()).isEqualTo(DEFAULT_LOGIN);
        assertThat(testAnUser.getPassword()).isEqualTo(DEFAULT_PASSWORD);
    }

    @Test
    @Transactional
    public void createAnUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = anUserRepository.findAll().size();

        // Create the AnUser with an existing ID
        anUser.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnUserMockMvc.perform(post("/api/an-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anUser)))
            .andExpect(status().isBadRequest());

        // Validate the AnUser in the database
        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = anUserRepository.findAll().size();
        // set the field null
        anUser.setUserId(null);

        // Create the AnUser, which fails.

        restAnUserMockMvc.perform(post("/api/an-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anUser)))
            .andExpect(status().isBadRequest());

        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLoginIsRequired() throws Exception {
        int databaseSizeBeforeTest = anUserRepository.findAll().size();
        // set the field null
        anUser.setLogin(null);

        // Create the AnUser, which fails.

        restAnUserMockMvc.perform(post("/api/an-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anUser)))
            .andExpect(status().isBadRequest());

        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPasswordIsRequired() throws Exception {
        int databaseSizeBeforeTest = anUserRepository.findAll().size();
        // set the field null
        anUser.setPassword(null);

        // Create the AnUser, which fails.

        restAnUserMockMvc.perform(post("/api/an-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anUser)))
            .andExpect(status().isBadRequest());

        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAnUsers() throws Exception {
        // Initialize the database
        anUserRepository.saveAndFlush(anUser);

        // Get all the anUserList
        restAnUserMockMvc.perform(get("/api/an-users?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(anUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].login").value(hasItem(DEFAULT_LOGIN)))
            .andExpect(jsonPath("$.[*].password").value(hasItem(DEFAULT_PASSWORD)));
    }
    
    @Test
    @Transactional
    public void getAnUser() throws Exception {
        // Initialize the database
        anUserRepository.saveAndFlush(anUser);

        // Get the anUser
        restAnUserMockMvc.perform(get("/api/an-users/{id}", anUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(anUser.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()))
            .andExpect(jsonPath("$.login").value(DEFAULT_LOGIN))
            .andExpect(jsonPath("$.password").value(DEFAULT_PASSWORD));
    }

    @Test
    @Transactional
    public void getNonExistingAnUser() throws Exception {
        // Get the anUser
        restAnUserMockMvc.perform(get("/api/an-users/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnUser() throws Exception {
        // Initialize the database
        anUserRepository.saveAndFlush(anUser);

        int databaseSizeBeforeUpdate = anUserRepository.findAll().size();

        // Update the anUser
        AnUser updatedAnUser = anUserRepository.findById(anUser.getId()).get();
        // Disconnect from session so that the updates on updatedAnUser are not directly saved in db
        em.detach(updatedAnUser);
        updatedAnUser
            .userId(UPDATED_USER_ID)
            .login(UPDATED_LOGIN)
            .password(UPDATED_PASSWORD);

        restAnUserMockMvc.perform(put("/api/an-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnUser)))
            .andExpect(status().isOk());

        // Validate the AnUser in the database
        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeUpdate);
        AnUser testAnUser = anUserList.get(anUserList.size() - 1);
        assertThat(testAnUser.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testAnUser.getLogin()).isEqualTo(UPDATED_LOGIN);
        assertThat(testAnUser.getPassword()).isEqualTo(UPDATED_PASSWORD);
    }

    @Test
    @Transactional
    public void updateNonExistingAnUser() throws Exception {
        int databaseSizeBeforeUpdate = anUserRepository.findAll().size();

        // Create the AnUser

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnUserMockMvc.perform(put("/api/an-users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anUser)))
            .andExpect(status().isBadRequest());

        // Validate the AnUser in the database
        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnUser() throws Exception {
        // Initialize the database
        anUserRepository.saveAndFlush(anUser);

        int databaseSizeBeforeDelete = anUserRepository.findAll().size();

        // Delete the anUser
        restAnUserMockMvc.perform(delete("/api/an-users/{id}", anUser.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AnUser> anUserList = anUserRepository.findAll();
        assertThat(anUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
