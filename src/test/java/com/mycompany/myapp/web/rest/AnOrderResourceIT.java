package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.CoopcycleApp;
import com.mycompany.myapp.domain.AnOrder;
import com.mycompany.myapp.repository.AnOrderRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AnOrderResource} REST controller.
 */
@SpringBootTest(classes = CoopcycleApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class AnOrderResourceIT {

    private static final Long DEFAULT_ORDER_ID = 1L;
    private static final Long UPDATED_ORDER_ID = 2L;

    @Autowired
    private AnOrderRepository anOrderRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAnOrderMockMvc;

    private AnOrder anOrder;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnOrder createEntity(EntityManager em) {
        AnOrder anOrder = new AnOrder()
            .orderId(DEFAULT_ORDER_ID);
        return anOrder;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AnOrder createUpdatedEntity(EntityManager em) {
        AnOrder anOrder = new AnOrder()
            .orderId(UPDATED_ORDER_ID);
        return anOrder;
    }

    @BeforeEach
    public void initTest() {
        anOrder = createEntity(em);
    }

    @Test
    @Transactional
    public void createAnOrder() throws Exception {
        int databaseSizeBeforeCreate = anOrderRepository.findAll().size();

        // Create the AnOrder
        restAnOrderMockMvc.perform(post("/api/an-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anOrder)))
            .andExpect(status().isCreated());

        // Validate the AnOrder in the database
        List<AnOrder> anOrderList = anOrderRepository.findAll();
        assertThat(anOrderList).hasSize(databaseSizeBeforeCreate + 1);
        AnOrder testAnOrder = anOrderList.get(anOrderList.size() - 1);
        assertThat(testAnOrder.getOrderId()).isEqualTo(DEFAULT_ORDER_ID);
    }

    @Test
    @Transactional
    public void createAnOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = anOrderRepository.findAll().size();

        // Create the AnOrder with an existing ID
        anOrder.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAnOrderMockMvc.perform(post("/api/an-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anOrder)))
            .andExpect(status().isBadRequest());

        // Validate the AnOrder in the database
        List<AnOrder> anOrderList = anOrderRepository.findAll();
        assertThat(anOrderList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkOrderIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = anOrderRepository.findAll().size();
        // set the field null
        anOrder.setOrderId(null);

        // Create the AnOrder, which fails.

        restAnOrderMockMvc.perform(post("/api/an-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anOrder)))
            .andExpect(status().isBadRequest());

        List<AnOrder> anOrderList = anOrderRepository.findAll();
        assertThat(anOrderList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllAnOrders() throws Exception {
        // Initialize the database
        anOrderRepository.saveAndFlush(anOrder);

        // Get all the anOrderList
        restAnOrderMockMvc.perform(get("/api/an-orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(anOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].orderId").value(hasItem(DEFAULT_ORDER_ID.intValue())));
    }
    
    @Test
    @Transactional
    public void getAnOrder() throws Exception {
        // Initialize the database
        anOrderRepository.saveAndFlush(anOrder);

        // Get the anOrder
        restAnOrderMockMvc.perform(get("/api/an-orders/{id}", anOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(anOrder.getId().intValue()))
            .andExpect(jsonPath("$.orderId").value(DEFAULT_ORDER_ID.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingAnOrder() throws Exception {
        // Get the anOrder
        restAnOrderMockMvc.perform(get("/api/an-orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAnOrder() throws Exception {
        // Initialize the database
        anOrderRepository.saveAndFlush(anOrder);

        int databaseSizeBeforeUpdate = anOrderRepository.findAll().size();

        // Update the anOrder
        AnOrder updatedAnOrder = anOrderRepository.findById(anOrder.getId()).get();
        // Disconnect from session so that the updates on updatedAnOrder are not directly saved in db
        em.detach(updatedAnOrder);
        updatedAnOrder
            .orderId(UPDATED_ORDER_ID);

        restAnOrderMockMvc.perform(put("/api/an-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAnOrder)))
            .andExpect(status().isOk());

        // Validate the AnOrder in the database
        List<AnOrder> anOrderList = anOrderRepository.findAll();
        assertThat(anOrderList).hasSize(databaseSizeBeforeUpdate);
        AnOrder testAnOrder = anOrderList.get(anOrderList.size() - 1);
        assertThat(testAnOrder.getOrderId()).isEqualTo(UPDATED_ORDER_ID);
    }

    @Test
    @Transactional
    public void updateNonExistingAnOrder() throws Exception {
        int databaseSizeBeforeUpdate = anOrderRepository.findAll().size();

        // Create the AnOrder

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAnOrderMockMvc.perform(put("/api/an-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(anOrder)))
            .andExpect(status().isBadRequest());

        // Validate the AnOrder in the database
        List<AnOrder> anOrderList = anOrderRepository.findAll();
        assertThat(anOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAnOrder() throws Exception {
        // Initialize the database
        anOrderRepository.saveAndFlush(anOrder);

        int databaseSizeBeforeDelete = anOrderRepository.findAll().size();

        // Delete the anOrder
        restAnOrderMockMvc.perform(delete("/api/an-orders/{id}", anOrder.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AnOrder> anOrderList = anOrderRepository.findAll();
        assertThat(anOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
