package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class AnUserTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnUser.class);
        AnUser anUser1 = new AnUser();
        anUser1.setId(1L);
        AnUser anUser2 = new AnUser();
        anUser2.setId(anUser1.getId());
        assertThat(anUser1).isEqualTo(anUser2);
        anUser2.setId(2L);
        assertThat(anUser1).isNotEqualTo(anUser2);
        anUser1.setId(null);
        assertThat(anUser1).isNotEqualTo(anUser2);
    }
}
