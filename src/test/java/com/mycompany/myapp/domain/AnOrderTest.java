package com.mycompany.myapp.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.mycompany.myapp.web.rest.TestUtil;

public class AnOrderTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AnOrder.class);
        AnOrder anOrder1 = new AnOrder();
        anOrder1.setId(1L);
        AnOrder anOrder2 = new AnOrder();
        anOrder2.setId(anOrder1.getId());
        assertThat(anOrder1).isEqualTo(anOrder2);
        anOrder2.setId(2L);
        assertThat(anOrder1).isNotEqualTo(anOrder2);
        anOrder1.setId(null);
        assertThat(anOrder1).isNotEqualTo(anOrder2);
    }
}
