import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AnUserComponentsPage, AnUserDeleteDialog, AnUserUpdatePage } from './an-user.page-object';

const expect = chai.expect;

describe('AnUser e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let anUserComponentsPage: AnUserComponentsPage;
  let anUserUpdatePage: AnUserUpdatePage;
  let anUserDeleteDialog: AnUserDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AnUsers', async () => {
    await navBarPage.goToEntity('an-user');
    anUserComponentsPage = new AnUserComponentsPage();
    await browser.wait(ec.visibilityOf(anUserComponentsPage.title), 5000);
    expect(await anUserComponentsPage.getTitle()).to.eq('coopcycleApp.anUser.home.title');
    await browser.wait(ec.or(ec.visibilityOf(anUserComponentsPage.entities), ec.visibilityOf(anUserComponentsPage.noResult)), 1000);
  });

  it('should load create AnUser page', async () => {
    await anUserComponentsPage.clickOnCreateButton();
    anUserUpdatePage = new AnUserUpdatePage();
    expect(await anUserUpdatePage.getPageTitle()).to.eq('coopcycleApp.anUser.home.createOrEditLabel');
    await anUserUpdatePage.cancel();
  });

  it('should create and save AnUsers', async () => {
    const nbButtonsBeforeCreate = await anUserComponentsPage.countDeleteButtons();

    await anUserComponentsPage.clickOnCreateButton();

    await promise.all([
      anUserUpdatePage.setUserIdInput('5'),
      anUserUpdatePage.setLoginInput('login'),
      anUserUpdatePage.setPasswordInput('password')
    ]);

    expect(await anUserUpdatePage.getUserIdInput()).to.eq('5', 'Expected userId value to be equals to 5');
    expect(await anUserUpdatePage.getLoginInput()).to.eq('login', 'Expected Login value to be equals to login');
    expect(await anUserUpdatePage.getPasswordInput()).to.eq('password', 'Expected Password value to be equals to password');

    await anUserUpdatePage.save();
    expect(await anUserUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await anUserComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last AnUser', async () => {
    const nbButtonsBeforeDelete = await anUserComponentsPage.countDeleteButtons();
    await anUserComponentsPage.clickOnLastDeleteButton();

    anUserDeleteDialog = new AnUserDeleteDialog();
    expect(await anUserDeleteDialog.getDialogTitle()).to.eq('coopcycleApp.anUser.delete.question');
    await anUserDeleteDialog.clickOnConfirmButton();

    expect(await anUserComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
