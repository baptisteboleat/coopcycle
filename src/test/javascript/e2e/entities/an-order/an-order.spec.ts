import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AnOrderComponentsPage, AnOrderDeleteDialog, AnOrderUpdatePage } from './an-order.page-object';

const expect = chai.expect;

describe('AnOrder e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let anOrderComponentsPage: AnOrderComponentsPage;
  let anOrderUpdatePage: AnOrderUpdatePage;
  let anOrderDeleteDialog: AnOrderDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AnOrders', async () => {
    await navBarPage.goToEntity('an-order');
    anOrderComponentsPage = new AnOrderComponentsPage();
    await browser.wait(ec.visibilityOf(anOrderComponentsPage.title), 5000);
    expect(await anOrderComponentsPage.getTitle()).to.eq('coopcycleApp.anOrder.home.title');
    await browser.wait(ec.or(ec.visibilityOf(anOrderComponentsPage.entities), ec.visibilityOf(anOrderComponentsPage.noResult)), 1000);
  });

  it('should load create AnOrder page', async () => {
    await anOrderComponentsPage.clickOnCreateButton();
    anOrderUpdatePage = new AnOrderUpdatePage();
    expect(await anOrderUpdatePage.getPageTitle()).to.eq('coopcycleApp.anOrder.home.createOrEditLabel');
    await anOrderUpdatePage.cancel();
  });

  it('should create and save AnOrders', async () => {
    const nbButtonsBeforeCreate = await anOrderComponentsPage.countDeleteButtons();

    await anOrderComponentsPage.clickOnCreateButton();

    await promise.all([anOrderUpdatePage.setOrderIdInput('5')]);

    expect(await anOrderUpdatePage.getOrderIdInput()).to.eq('5', 'Expected orderId value to be equals to 5');

    await anOrderUpdatePage.save();
    expect(await anOrderUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await anOrderComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last AnOrder', async () => {
    const nbButtonsBeforeDelete = await anOrderComponentsPage.countDeleteButtons();
    await anOrderComponentsPage.clickOnLastDeleteButton();

    anOrderDeleteDialog = new AnOrderDeleteDialog();
    expect(await anOrderDeleteDialog.getDialogTitle()).to.eq('coopcycleApp.anOrder.delete.question');
    await anOrderDeleteDialog.clickOnConfirmButton();

    expect(await anOrderComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
