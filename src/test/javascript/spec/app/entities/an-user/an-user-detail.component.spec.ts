import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoopcycleTestModule } from '../../../test.module';
import { AnUserDetailComponent } from 'app/entities/an-user/an-user-detail.component';
import { AnUser } from 'app/shared/model/an-user.model';

describe('Component Tests', () => {
  describe('AnUser Management Detail Component', () => {
    let comp: AnUserDetailComponent;
    let fixture: ComponentFixture<AnUserDetailComponent>;
    const route = ({ data: of({ anUser: new AnUser(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoopcycleTestModule],
        declarations: [AnUserDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AnUserDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AnUserDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load anUser on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.anUser).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
