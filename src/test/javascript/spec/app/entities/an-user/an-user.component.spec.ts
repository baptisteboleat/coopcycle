import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CoopcycleTestModule } from '../../../test.module';
import { AnUserComponent } from 'app/entities/an-user/an-user.component';
import { AnUserService } from 'app/entities/an-user/an-user.service';
import { AnUser } from 'app/shared/model/an-user.model';

describe('Component Tests', () => {
  describe('AnUser Management Component', () => {
    let comp: AnUserComponent;
    let fixture: ComponentFixture<AnUserComponent>;
    let service: AnUserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoopcycleTestModule],
        declarations: [AnUserComponent]
      })
        .overrideTemplate(AnUserComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AnUserComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AnUserService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AnUser(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.anUsers && comp.anUsers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
