import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { CoopcycleTestModule } from '../../../test.module';
import { AnUserUpdateComponent } from 'app/entities/an-user/an-user-update.component';
import { AnUserService } from 'app/entities/an-user/an-user.service';
import { AnUser } from 'app/shared/model/an-user.model';

describe('Component Tests', () => {
  describe('AnUser Management Update Component', () => {
    let comp: AnUserUpdateComponent;
    let fixture: ComponentFixture<AnUserUpdateComponent>;
    let service: AnUserService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoopcycleTestModule],
        declarations: [AnUserUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(AnUserUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AnUserUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AnUserService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new AnUser(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new AnUser();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
