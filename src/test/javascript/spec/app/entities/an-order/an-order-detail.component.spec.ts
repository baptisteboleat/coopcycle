import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoopcycleTestModule } from '../../../test.module';
import { AnOrderDetailComponent } from 'app/entities/an-order/an-order-detail.component';
import { AnOrder } from 'app/shared/model/an-order.model';

describe('Component Tests', () => {
  describe('AnOrder Management Detail Component', () => {
    let comp: AnOrderDetailComponent;
    let fixture: ComponentFixture<AnOrderDetailComponent>;
    const route = ({ data: of({ anOrder: new AnOrder(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoopcycleTestModule],
        declarations: [AnOrderDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(AnOrderDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(AnOrderDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load anOrder on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.anOrder).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
