import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { CoopcycleTestModule } from '../../../test.module';
import { AnOrderComponent } from 'app/entities/an-order/an-order.component';
import { AnOrderService } from 'app/entities/an-order/an-order.service';
import { AnOrder } from 'app/shared/model/an-order.model';

describe('Component Tests', () => {
  describe('AnOrder Management Component', () => {
    let comp: AnOrderComponent;
    let fixture: ComponentFixture<AnOrderComponent>;
    let service: AnOrderService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [CoopcycleTestModule],
        declarations: [AnOrderComponent]
      })
        .overrideTemplate(AnOrderComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AnOrderComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AnOrderService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AnOrder(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.anOrders && comp.anOrders[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
