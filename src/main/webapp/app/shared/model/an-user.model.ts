export interface IAnUser {
  id?: number;
  userId?: number;
  login?: string;
  password?: string;
}

export class AnUser implements IAnUser {
  constructor(public id?: number, public userId?: number, public login?: string, public password?: string) {}
}
