export interface IAnOrder {
  id?: number;
  orderId?: number;
}

export class AnOrder implements IAnOrder {
  constructor(public id?: number, public orderId?: number) {}
}
