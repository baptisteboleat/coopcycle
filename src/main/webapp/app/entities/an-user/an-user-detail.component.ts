import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAnUser } from 'app/shared/model/an-user.model';

@Component({
  selector: 'jhi-an-user-detail',
  templateUrl: './an-user-detail.component.html'
})
export class AnUserDetailComponent implements OnInit {
  anUser: IAnUser | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ anUser }) => (this.anUser = anUser));
  }

  previousState(): void {
    window.history.back();
  }
}
