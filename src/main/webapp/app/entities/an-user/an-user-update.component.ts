import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAnUser, AnUser } from 'app/shared/model/an-user.model';
import { AnUserService } from './an-user.service';

@Component({
  selector: 'jhi-an-user-update',
  templateUrl: './an-user-update.component.html'
})
export class AnUserUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userId: [null, [Validators.required]],
    login: [null, [Validators.required]],
    password: [null, [Validators.required]]
  });

  constructor(protected anUserService: AnUserService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ anUser }) => {
      this.updateForm(anUser);
    });
  }

  updateForm(anUser: IAnUser): void {
    this.editForm.patchValue({
      id: anUser.id,
      userId: anUser.userId,
      login: anUser.login,
      password: anUser.password
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const anUser = this.createFromForm();
    if (anUser.id !== undefined) {
      this.subscribeToSaveResponse(this.anUserService.update(anUser));
    } else {
      this.subscribeToSaveResponse(this.anUserService.create(anUser));
    }
  }

  private createFromForm(): IAnUser {
    return {
      ...new AnUser(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      login: this.editForm.get(['login'])!.value,
      password: this.editForm.get(['password'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnUser>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
