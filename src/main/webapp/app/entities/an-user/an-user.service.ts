import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAnUser } from 'app/shared/model/an-user.model';

type EntityResponseType = HttpResponse<IAnUser>;
type EntityArrayResponseType = HttpResponse<IAnUser[]>;

@Injectable({ providedIn: 'root' })
export class AnUserService {
  public resourceUrl = SERVER_API_URL + 'api/an-users';

  constructor(protected http: HttpClient) {}

  create(anUser: IAnUser): Observable<EntityResponseType> {
    return this.http.post<IAnUser>(this.resourceUrl, anUser, { observe: 'response' });
  }

  update(anUser: IAnUser): Observable<EntityResponseType> {
    return this.http.put<IAnUser>(this.resourceUrl, anUser, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAnUser>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAnUser[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
