import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAnUser, AnUser } from 'app/shared/model/an-user.model';
import { AnUserService } from './an-user.service';
import { AnUserComponent } from './an-user.component';
import { AnUserDetailComponent } from './an-user-detail.component';
import { AnUserUpdateComponent } from './an-user-update.component';

@Injectable({ providedIn: 'root' })
export class AnUserResolve implements Resolve<IAnUser> {
  constructor(private service: AnUserService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAnUser> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((anUser: HttpResponse<AnUser>) => {
          if (anUser.body) {
            return of(anUser.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AnUser());
  }
}

export const anUserRoute: Routes = [
  {
    path: '',
    component: AnUserComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anUser.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AnUserDetailComponent,
    resolve: {
      anUser: AnUserResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anUser.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AnUserUpdateComponent,
    resolve: {
      anUser: AnUserResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anUser.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AnUserUpdateComponent,
    resolve: {
      anUser: AnUserResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anUser.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
