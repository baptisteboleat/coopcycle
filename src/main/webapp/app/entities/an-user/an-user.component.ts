import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAnUser } from 'app/shared/model/an-user.model';
import { AnUserService } from './an-user.service';
import { AnUserDeleteDialogComponent } from './an-user-delete-dialog.component';

@Component({
  selector: 'jhi-an-user',
  templateUrl: './an-user.component.html'
})
export class AnUserComponent implements OnInit, OnDestroy {
  anUsers?: IAnUser[];
  eventSubscriber?: Subscription;

  constructor(protected anUserService: AnUserService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.anUserService.query().subscribe((res: HttpResponse<IAnUser[]>) => (this.anUsers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAnUsers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAnUser): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAnUsers(): void {
    this.eventSubscriber = this.eventManager.subscribe('anUserListModification', () => this.loadAll());
  }

  delete(anUser: IAnUser): void {
    const modalRef = this.modalService.open(AnUserDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.anUser = anUser;
  }
}
