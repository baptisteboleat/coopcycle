import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoopcycleSharedModule } from 'app/shared/shared.module';
import { AnUserComponent } from './an-user.component';
import { AnUserDetailComponent } from './an-user-detail.component';
import { AnUserUpdateComponent } from './an-user-update.component';
import { AnUserDeleteDialogComponent } from './an-user-delete-dialog.component';
import { anUserRoute } from './an-user.route';

@NgModule({
  imports: [CoopcycleSharedModule, RouterModule.forChild(anUserRoute)],
  declarations: [AnUserComponent, AnUserDetailComponent, AnUserUpdateComponent, AnUserDeleteDialogComponent],
  entryComponents: [AnUserDeleteDialogComponent]
})
export class CoopcycleAnUserModule {}
