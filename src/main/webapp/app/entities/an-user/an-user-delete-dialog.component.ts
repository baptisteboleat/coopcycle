import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAnUser } from 'app/shared/model/an-user.model';
import { AnUserService } from './an-user.service';

@Component({
  templateUrl: './an-user-delete-dialog.component.html'
})
export class AnUserDeleteDialogComponent {
  anUser?: IAnUser;

  constructor(protected anUserService: AnUserService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.anUserService.delete(id).subscribe(() => {
      this.eventManager.broadcast('anUserListModification');
      this.activeModal.close();
    });
  }
}
