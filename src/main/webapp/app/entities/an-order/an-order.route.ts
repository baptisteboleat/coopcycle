import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IAnOrder, AnOrder } from 'app/shared/model/an-order.model';
import { AnOrderService } from './an-order.service';
import { AnOrderComponent } from './an-order.component';
import { AnOrderDetailComponent } from './an-order-detail.component';
import { AnOrderUpdateComponent } from './an-order-update.component';

@Injectable({ providedIn: 'root' })
export class AnOrderResolve implements Resolve<IAnOrder> {
  constructor(private service: AnOrderService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAnOrder> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((anOrder: HttpResponse<AnOrder>) => {
          if (anOrder.body) {
            return of(anOrder.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new AnOrder());
  }
}

export const anOrderRoute: Routes = [
  {
    path: '',
    component: AnOrderComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anOrder.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AnOrderDetailComponent,
    resolve: {
      anOrder: AnOrderResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anOrder.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AnOrderUpdateComponent,
    resolve: {
      anOrder: AnOrderResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anOrder.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AnOrderUpdateComponent,
    resolve: {
      anOrder: AnOrderResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'coopcycleApp.anOrder.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
