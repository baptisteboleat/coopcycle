import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IAnOrder, AnOrder } from 'app/shared/model/an-order.model';
import { AnOrderService } from './an-order.service';

@Component({
  selector: 'jhi-an-order-update',
  templateUrl: './an-order-update.component.html'
})
export class AnOrderUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    orderId: [null, [Validators.required]]
  });

  constructor(protected anOrderService: AnOrderService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ anOrder }) => {
      this.updateForm(anOrder);
    });
  }

  updateForm(anOrder: IAnOrder): void {
    this.editForm.patchValue({
      id: anOrder.id,
      orderId: anOrder.orderId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const anOrder = this.createFromForm();
    if (anOrder.id !== undefined) {
      this.subscribeToSaveResponse(this.anOrderService.update(anOrder));
    } else {
      this.subscribeToSaveResponse(this.anOrderService.create(anOrder));
    }
  }

  private createFromForm(): IAnOrder {
    return {
      ...new AnOrder(),
      id: this.editForm.get(['id'])!.value,
      orderId: this.editForm.get(['orderId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAnOrder>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
