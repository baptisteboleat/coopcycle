import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IAnOrder } from 'app/shared/model/an-order.model';
import { AnOrderService } from './an-order.service';

@Component({
  templateUrl: './an-order-delete-dialog.component.html'
})
export class AnOrderDeleteDialogComponent {
  anOrder?: IAnOrder;

  constructor(protected anOrderService: AnOrderService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.anOrderService.delete(id).subscribe(() => {
      this.eventManager.broadcast('anOrderListModification');
      this.activeModal.close();
    });
  }
}
