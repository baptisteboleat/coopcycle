import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAnOrder } from 'app/shared/model/an-order.model';
import { AnOrderService } from './an-order.service';
import { AnOrderDeleteDialogComponent } from './an-order-delete-dialog.component';

@Component({
  selector: 'jhi-an-order',
  templateUrl: './an-order.component.html'
})
export class AnOrderComponent implements OnInit, OnDestroy {
  anOrders?: IAnOrder[];
  eventSubscriber?: Subscription;

  constructor(protected anOrderService: AnOrderService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.anOrderService.query().subscribe((res: HttpResponse<IAnOrder[]>) => (this.anOrders = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAnOrders();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAnOrder): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAnOrders(): void {
    this.eventSubscriber = this.eventManager.subscribe('anOrderListModification', () => this.loadAll());
  }

  delete(anOrder: IAnOrder): void {
    const modalRef = this.modalService.open(AnOrderDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.anOrder = anOrder;
  }
}
