import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoopcycleSharedModule } from 'app/shared/shared.module';
import { AnOrderComponent } from './an-order.component';
import { AnOrderDetailComponent } from './an-order-detail.component';
import { AnOrderUpdateComponent } from './an-order-update.component';
import { AnOrderDeleteDialogComponent } from './an-order-delete-dialog.component';
import { anOrderRoute } from './an-order.route';

@NgModule({
  imports: [CoopcycleSharedModule, RouterModule.forChild(anOrderRoute)],
  declarations: [AnOrderComponent, AnOrderDetailComponent, AnOrderUpdateComponent, AnOrderDeleteDialogComponent],
  entryComponents: [AnOrderDeleteDialogComponent]
})
export class CoopcycleAnOrderModule {}
