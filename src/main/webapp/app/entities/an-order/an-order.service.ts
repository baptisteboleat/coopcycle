import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IAnOrder } from 'app/shared/model/an-order.model';

type EntityResponseType = HttpResponse<IAnOrder>;
type EntityArrayResponseType = HttpResponse<IAnOrder[]>;

@Injectable({ providedIn: 'root' })
export class AnOrderService {
  public resourceUrl = SERVER_API_URL + 'api/an-orders';

  constructor(protected http: HttpClient) {}

  create(anOrder: IAnOrder): Observable<EntityResponseType> {
    return this.http.post<IAnOrder>(this.resourceUrl, anOrder, { observe: 'response' });
  }

  update(anOrder: IAnOrder): Observable<EntityResponseType> {
    return this.http.put<IAnOrder>(this.resourceUrl, anOrder, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAnOrder>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAnOrder[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
