import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAnOrder } from 'app/shared/model/an-order.model';

@Component({
  selector: 'jhi-an-order-detail',
  templateUrl: './an-order-detail.component.html'
})
export class AnOrderDetailComponent implements OnInit {
  anOrder: IAnOrder | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ anOrder }) => (this.anOrder = anOrder));
  }

  previousState(): void {
    window.history.back();
  }
}
