package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.AnOrder;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AnOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnOrderRepository extends JpaRepository<AnOrder, Long> {
}
