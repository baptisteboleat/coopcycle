package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.AnUser;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AnUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AnUserRepository extends JpaRepository<AnUser, Long> {
}
