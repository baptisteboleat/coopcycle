package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.AnOrder;
import com.mycompany.myapp.repository.AnOrderRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.AnOrder}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AnOrderResource {

    private final Logger log = LoggerFactory.getLogger(AnOrderResource.class);

    private static final String ENTITY_NAME = "anOrder";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AnOrderRepository anOrderRepository;

    public AnOrderResource(AnOrderRepository anOrderRepository) {
        this.anOrderRepository = anOrderRepository;
    }

    /**
     * {@code POST  /an-orders} : Create a new anOrder.
     *
     * @param anOrder the anOrder to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new anOrder, or with status {@code 400 (Bad Request)} if the anOrder has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/an-orders")
    public ResponseEntity<AnOrder> createAnOrder(@Valid @RequestBody AnOrder anOrder) throws URISyntaxException {
        log.debug("REST request to save AnOrder : {}", anOrder);
        if (anOrder.getId() != null) {
            throw new BadRequestAlertException("A new anOrder cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AnOrder result = anOrderRepository.save(anOrder);
        return ResponseEntity.created(new URI("/api/an-orders/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /an-orders} : Updates an existing anOrder.
     *
     * @param anOrder the anOrder to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated anOrder,
     * or with status {@code 400 (Bad Request)} if the anOrder is not valid,
     * or with status {@code 500 (Internal Server Error)} if the anOrder couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/an-orders")
    public ResponseEntity<AnOrder> updateAnOrder(@Valid @RequestBody AnOrder anOrder) throws URISyntaxException {
        log.debug("REST request to update AnOrder : {}", anOrder);
        if (anOrder.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AnOrder result = anOrderRepository.save(anOrder);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, anOrder.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /an-orders} : get all the anOrders.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of anOrders in body.
     */
    @GetMapping("/an-orders")
    public List<AnOrder> getAllAnOrders() {
        log.debug("REST request to get all AnOrders");
        return anOrderRepository.findAll();
    }

    /**
     * {@code GET  /an-orders/:id} : get the "id" anOrder.
     *
     * @param id the id of the anOrder to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the anOrder, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/an-orders/{id}")
    public ResponseEntity<AnOrder> getAnOrder(@PathVariable Long id) {
        log.debug("REST request to get AnOrder : {}", id);
        Optional<AnOrder> anOrder = anOrderRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(anOrder);
    }

    /**
     * {@code DELETE  /an-orders/:id} : delete the "id" anOrder.
     *
     * @param id the id of the anOrder to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/an-orders/{id}")
    public ResponseEntity<Void> deleteAnOrder(@PathVariable Long id) {
        log.debug("REST request to delete AnOrder : {}", id);
        anOrderRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
