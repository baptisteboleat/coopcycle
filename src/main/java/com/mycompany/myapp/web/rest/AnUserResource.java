package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.AnUser;
import com.mycompany.myapp.repository.AnUserRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.AnUser}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AnUserResource {

    private final Logger log = LoggerFactory.getLogger(AnUserResource.class);

    private static final String ENTITY_NAME = "anUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AnUserRepository anUserRepository;

    public AnUserResource(AnUserRepository anUserRepository) {
        this.anUserRepository = anUserRepository;
    }

    /**
     * {@code POST  /an-users} : Create a new anUser.
     *
     * @param anUser the anUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new anUser, or with status {@code 400 (Bad Request)} if the anUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/an-users")
    public ResponseEntity<AnUser> createAnUser(@Valid @RequestBody AnUser anUser) throws URISyntaxException {
        log.debug("REST request to save AnUser : {}", anUser);
        if (anUser.getId() != null) {
            throw new BadRequestAlertException("A new anUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AnUser result = anUserRepository.save(anUser);
        return ResponseEntity.created(new URI("/api/an-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /an-users} : Updates an existing anUser.
     *
     * @param anUser the anUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated anUser,
     * or with status {@code 400 (Bad Request)} if the anUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the anUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/an-users")
    public ResponseEntity<AnUser> updateAnUser(@Valid @RequestBody AnUser anUser) throws URISyntaxException {
        log.debug("REST request to update AnUser : {}", anUser);
        if (anUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AnUser result = anUserRepository.save(anUser);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, anUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /an-users} : get all the anUsers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of anUsers in body.
     */
    @GetMapping("/an-users")
    public List<AnUser> getAllAnUsers() {
        log.debug("REST request to get all AnUsers");
        return anUserRepository.findAll();
    }

    /**
     * {@code GET  /an-users/:id} : get the "id" anUser.
     *
     * @param id the id of the anUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the anUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/an-users/{id}")
    public ResponseEntity<AnUser> getAnUser(@PathVariable Long id) {
        log.debug("REST request to get AnUser : {}", id);
        Optional<AnUser> anUser = anUserRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(anUser);
    }

    /**
     * {@code DELETE  /an-users/:id} : delete the "id" anUser.
     *
     * @param id the id of the anUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/an-users/{id}")
    public ResponseEntity<Void> deleteAnUser(@PathVariable Long id) {
        log.debug("REST request to delete AnUser : {}", id);
        anUserRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
